package com.mahjoubi.networksample

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.mahjoubi.networksample.networking.*
import com.squareup.moshi.JsonAdapter
import kotlinx.coroutines.async
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class MainActivity2 : AppCompatActivity() {

    var baseUrl = "https://api.tapsell.ir/v2/location/european/"

    var baseUrl2 = "https://cc442df0-f8cf-41b2-8e95-1d0f74d98118.mock.pstmn.io/tapsell.mock/adnetworks/"

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)

        val requestoptions = Requestoptions().apply {
            priority = Priority.IMMEDIATE
            retry = 3
        }


        val retrofitSampleApi = NetworkUtils.retrofit.create(RetrofitSampleApi::class.java)

        val networkRequest = NetworkRequest(requestoptions,
            retrofitSampleApi.getSamples(baseUrl2),
            SampleEntity::class.java){

            it.setHandlerForResult(::handle_Failer , ::handle_response)
        }


        val manager = NetworkManager()


        tapsellCoroutineScope.launch {
            Log.e("Co" , "inside")

            async {
                Log.e("ResponseOBS", "first response Observer attached")

                manager.addObserverToResposneQueue(this) {
                    Log.e("ResponseOBS", "first response Observer collect data")

                }

            }
            async {
                delay(5000)

                Log.e("ResponseOBS", "second response Observer attached")

                manager.addObserverToResposneQueue(this) {
                    Log.e("ResponseOBS", "second response Observer collect data")


                }

            }


            async {
                Log.e("RequestOBS" , "first request Observer attached")

                manager.addObserverToRequestsQueue(this ){
                    Log.e("RequestOBS" , "first request Observer collect data")

                }
            }





                async {
                    manager.emitToRequestQueue(this, networkRequest)
                }







        }


    }


    fun handle_response(outputData: Any?) {

        val jsonAdapter: JsonAdapter<SampleEntity> = NetworkUtils.getMoshi().adapter(SampleEntity::class.java)

        val sampleEntity = outputData as SampleEntity

        var answer = jsonAdapter.toJson(sampleEntity)

        Log.e("responseSuccess", answer)
    }



    fun handle_Failer(string: String?) {
        Log.e("responseFaile", "enter")


        if (string != null) {
            Log.e("responseFaile", string)
        } else {
            Log.e("responseFaile", "string")
        }
    }
}