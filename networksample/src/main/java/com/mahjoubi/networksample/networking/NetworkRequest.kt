package com.mahjoubi.networksample.networking

import com.squareup.moshi.JsonAdapter
import retrofit2.Call

data class  NetworkRequest(
    var requestoptions: Requestoptions,
    var call: Call<*>,
    var responseType:Class<*>,
    var responseHandler: (NetworkingEither<String?, *>) -> Unit

) {


    fun  getJsonAdapter(): JsonAdapter<*> {

        val adapter = NetworkUtils.getMoshi().adapter(responseType)

        return adapter

    }

}