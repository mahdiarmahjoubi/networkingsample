package com.mahjoubi.networksample.networking

import com.squareup.moshi.Moshi
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object NetworkUtils {
        const val API_BASE_URL = "https://cc442df0-f8cf-41b2-8e95-1d0f74d98118.mock.pstmn.io/tapsell.mock/adnetworks/"


    var retrofit: Retrofit = Retrofit.Builder()
        .baseUrl(API_BASE_URL)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    fun getMoshi(): Moshi {
        val moshi: Moshi = Moshi.Builder().build()

        return moshi;

    }
}