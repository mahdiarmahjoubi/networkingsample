package com.mahjoubi.networksample.networking

import com.squareup.moshi.Json

class SampleEntity {
    @Json(name ="adNetworks")
    var adNetworks: List<SampleUnitEntity?>? = null

    class SampleUnitEntity {
        @Json(name="name")
        var name: String? = null

        @Json(name="id")
        var id: String? = null
    }
}