package com.mahjoubi.networksample.networking

import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import retrofit2.Response


/**
 * this class helps us to get RemoteData
 * we Use Retrofit Call as parameter to get data
 * also we use Coroutine and ShareFlow Concepts in handling request
 * this class use these dependencies that get from user to start its work
 * @see NetworkRequest
 * @see CoroutineScope
 * also it return this sealed class(Either<String?, *>) as response to user for decide.
 * first parameter of it is error message and second parameter is body of response
 * @see Either
 */
class NetworkManager {

    /**
     * in this part we share tow methods for user to add handler
     * when each request or response emit
     */


    /**
     * responseFlow is like Broadcast to notify receivers we get new response to handle
     * their work that assign to it
     * @see addObserverToResposneQueue
     */
    val _responseFlow = MutableSharedFlow<NetworkingEither<String?, *>>(replay = 0)
    var responseFlow = _responseFlow.asSharedFlow()

    fun addObserverToResposneQueue(coroutineScope: CoroutineScope, operation: (NetworkingEither<String?, *>) -> Unit) {

        coroutineScope.launch {

            responseFlow.collect {

                operation(it)
            }
        }


    }

    /**
     * requsetFlow is like Broadcast to notify receivers we get new request to handle
     * their work that assign to it
     * @see addObserverToRequestsQueue
     */
    val _requestFlow = MutableSharedFlow<Pair<CoroutineScope, NetworkRequest>>(replay = 0)
    val requestFlow = _requestFlow.asSharedFlow()

    fun addObserverToRequestsQueue(coroutineScope: CoroutineScope, operation: (NetworkRequest) -> Unit) {
        coroutineScope.launch {
            requestFlow.collect {

                operation(it.second)
            }
        }
    }



    init {
        createNetworkCallerReceiver()
    }


    /**
     * helper method to get correct CoroutineScope with Priority
     * @see Priority
     * @see Requestoptions
     */
    fun getCoroutinScopesRepository() = mapOf(
        Priority.IMMEDIATE to immediateNetworkCoroutineScope,
        Priority.MEDIUM to generalNetworkCoroutineScope
    )

    /**
     * receiver that defines in [createNetworkCallerReceiver] call this method when request collect
     */
    suspend fun callNetwork(coroutineScope: CoroutineScope, networkRequest: NetworkRequest
    ): Flow<NetworkingEither<String?, *>> {

        // get correct scope with priority key
        val networkCoScope = getCoroutinScopesRepository().get(networkRequest.requestoptions.priority)!!


        //CoroutineScope that request send in it
        val launchingCoScope = coroutineScope




        // decide task run in witch Scope
        val result = launchSpecificNetworkScope(networkCoScope) {
            val response = networkRequest.call.clone().execute()

            val data = handleResponse(response,
                networkRequest,
                launchingCoScope)


            return@launchSpecificNetworkScope data

        }

        return result


    }
    /**
     * when response is not successful,
     * we first check have we sequence number in request options to retry networking or not
     * also we use some Extension to parse error (Exception or errorBody)
     *
     * @see Throwable.parseException
     * @see Response.parseError
     *
     * ## WIP
     * NOTE: above utility is under developing, we return simple string until now
     */
    fun handleResponse(response: Response<*>,
                       networkRequest: NetworkRequest,
                       coroutineScope: CoroutineScope
    ) : NetworkingEither<String?, *> {

        var result:NetworkingEither<String?, *> = NetworkingEither.Left("")

        try {
            if (response.isSuccessful) {

                val answer = response.body().toString()

                val data = networkRequest.getJsonAdapter().fromJson(answer)

                emitToResponseFlow(coroutineScope, NetworkingEither.Right(data))

                result = NetworkingEither.Right(data)

            } else {
                if (networkRequest.requestoptions.getsequenceNumber() > 0) {

                    Log.e("retry", networkRequest.requestoptions.getsequenceNumber().toString())

                    networkRequest.requestoptions.tryAgain()

                    result = handleResponse(response, networkRequest, coroutineScope)

                } else {

                    emitToResponseFlow(coroutineScope, NetworkingEither.Left(response.parseError()))

                    result = NetworkingEither.Left(response.parseError())

                }
            }
        } catch (exeption: Exception) {
            if (networkRequest.requestoptions.getsequenceNumber() > 0) {

                Log.e("retry", networkRequest.requestoptions.getsequenceNumber().toString())

                networkRequest.requestoptions.tryAgain()

                result = handleResponse(response, networkRequest, coroutineScope)

            } else {

                emitToResponseFlow(coroutineScope, NetworkingEither.Left(exeption.parseException()))

                result = NetworkingEither.Left(exeption.parseException())
            }
        }

        return result

    }


    fun emitToResponseFlow(coroutineScope: CoroutineScope, either: NetworkingEither<String?, *>) {

        coroutineScope.launch {

            _responseFlow.emit(either)

            responseFlow.collect()

        }

    }

    /**
     * this method use when we want send request to request queue
     * it emit to requestFlow to collect it receivers add to it
     */
    suspend fun emitToRequestQueue(coroutineScope: CoroutineScope, networkRequest: NetworkRequest) {
        val Object = Pair(coroutineScope, networkRequest)
        coroutineScope.launch {
            _requestFlow.emit(Object)
            requestFlow.collect()
        }

    }

    /**
     * call in init block of class to add receiver to requestFlow
     * and call network when collect it
     */
    fun createNetworkCallerReceiver(){
        tapsellCoroutineScope.launch {
            requestFlow
                .debounce(1000)
                .collect {
                    val responseHandler = it.second.responseHandler
                    callNetwork(it.first, it.second).collect {
                        responseHandler(it)
                    }
                }
        }
    }



}