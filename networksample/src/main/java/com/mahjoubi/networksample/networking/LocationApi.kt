package com.mahjoubi.networksample.networking

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Url

interface LocationApi {

    @GET("")
    fun getSamples(@Url url: String?): Call<LocationEuropean>
}