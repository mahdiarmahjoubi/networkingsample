package com.mahjoubi.networksample.networking

import retrofit2.Response


fun Throwable.parseException(
): String? {

    return this.localizedMessage


}


fun <T> Response<T>.parseError(

): String {

    val message = "code : " + this.raw().code().toString() + " message : " + this.message()
    return message


}