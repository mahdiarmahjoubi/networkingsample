package com.mahjoubi.networksample.networking

import kotlinx.coroutines.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

/**
 * This class provides utilities to work with coroutines easily and safely.
 *
 * **Internal Docs**: `guide/dev/coroutines.md`
 */
private const val tapsellTag = "Tapsell"

/**
 * ## WIP implementation
 *
 * Main executor of the tapsell SDK. This is used to run our code on
 *
 */
private val tapsellExecutor: ExecutorService = Executors.newSingleThreadExecutor()



/**
 * ## WIP Implementation
 * This executor should be used for call Api with medium or low [Priority]
 *
 */
private val generalNetworkExecutor: ExecutorService = Executors.newSingleThreadExecutor()

/**
 * ## WIP Implementation
 * This executor should be used for call Api with High [Priority]
 */
private val immediateNetworkExecutor: ExecutorService = Executors.newSingleThreadExecutor()

/**
 * ## WIP implementation
 *
 * TapsellDispatcher to run coroutines on.
 */
val tapsellCoroutineDispatcher = tapsellExecutor.asCoroutineDispatcher()


/**
 * ## WIP Implementation
 * This dispatcher is created by using the [generalNetworkExecutor].
 *
 *
 * Should be used to launch general networking in coroutines
 *
 * To launch a io coroutines, use [generalNetworkCoroutineScope]
 *
 * To move op to io coroutine (`withContext`) use [generalNetworkScope]
 */
val generalNetworkDispatcher = generalNetworkExecutor.asCoroutineDispatcher()


/**
 * ## WIP Implementation
 * This dispatcher is created by using the [immediateNetworkExecutor].
 *
 *
 * Should be used to launch general networking in coroutines
 *
 * To launch a io coroutines, use [immediateNetworkCoroutineScope]
 *
 * To move op to io coroutine (`withContext`) use [immediateNetworkScope]
 */
val immediateNetworkDispatcher = generalNetworkExecutor.asCoroutineDispatcher()


/**
 * ## WIP implementation
 * The main and must-use scope for tapsell related coroutines.
 * If you want to launch a coroutine, you need to use this scope.
 *
 * **NOTE**: To use it prefer using [tapsellScope] function or [launchInTapsellScope]
 */
val tapsellCoroutineScope = CoroutineScope(context = tapsellCoroutineDispatcher + SupervisorJob())

/**
 * ## WIP Implementation
 * CoroutineScope for general networking
 *
 * Uses supervisor job to handle unexpected canceling and avoid self-shutdown and children cancellation
 */
val generalNetworkCoroutineScope = CoroutineScope(context = generalNetworkDispatcher + SupervisorJob())

/**
 * ## WIP Implementation
 * CoroutineScope for immediate networking
 *
 * Uses supervisor job to handle unexpected canceling and avoid self-shutdown and children cancellation
 */
val immediateNetworkCoroutineScope = CoroutineScope(context = immediateNetworkDispatcher + SupervisorJob())


/**
 * ## WIP implementation
 * A util function to run suspendable coroutines on.
 *
 * **NOTE**: Prefer this over regular usage, since this is `MainSafe`. If you use it on a main coroutine, it will not interrupt the main thread.
 */
suspend inline fun <T> tapsellScope(crossinline op: suspend CoroutineScope.() -> T): T =
    withContext(tapsellCoroutineDispatcher) {
        op()
    }

/**
 * ## WIP Implementation
 * A util function to move coroutines to io
 *
 * **NOTE** prefer this over regular `withContext`.
 */
suspend inline fun <T> generalNetworkScope(crossinline op: suspend CoroutineScope.() -> T): T = withContext(
    generalNetworkDispatcher
) { op() }


/**
 * ## WIP Implementation
 * A util function to move coroutines to ui thread
 *
 * **NOTE** prefer this over regular `withContext`.
 */
suspend inline fun <T> immediateNetworkScope(crossinline op: suspend CoroutineScope.() -> T): T = withContext(
    immediateNetworkDispatcher
) { op() }

/**
 * ## WIP implementation
 * Run background code on our thread.
 *
 * ## Usage
 *
 * ```
 * launchInTapsellScope { (this: CoroutineScope) -> }
 * ```
 *
 * Prefer this over regular launch since this is error safe. If any errors happen this will notify our crash handler
 */
inline fun launchInTapsellScope(crossinline op: suspend CoroutineScope.() -> Unit) =
    tapsellCoroutineScope
        .launch {
            op()
        }



/**
 * ## WIP implementation
 * Run background code for general networking on our thread.
 *
 * ## Usage
 *
 * ```
 * launchIngeneralNetworkScope { (this: CoroutineScope) -> }
 * ```
 *
 * Prefer this over regular launch since this is error safe. If any errors happen this will notify our crash handler
 */
inline fun launchIngeneralNetworkScope(crossinline op: suspend CoroutineScope.()->Unit) =
    generalNetworkCoroutineScope
        .launch {
            op()
        }


/**
 * ## WIP Implementation
 * Run background code for immediate networking on other thread.
 *
 * ## Usage
 *
 * ```
 * launchInimmediateNetworkScope { (this: CoroutineScope) -> }
 * ```
 *
 * Prefer this over regular launch since this is error safe. If any errors happen this will notify our crash handler
 *
 */
inline fun launchInimmediateNetworkScope(crossinline op: suspend CoroutineScope.()->Unit) =
    immediateNetworkCoroutineScope
        .launch {
            op()
        }




/**
 * ## WIP Implementation
 * Run background code for specific scope on other thread.
 *
 * ## Usage
 *
 * ```
 * launchInimmediateNetworkScope { (this: CoroutineScope) -> }
 * ```
 *
 * Prefer this over regular launch since this is error safe. If any errors happen this will notify our crash handler
 *
 */
suspend inline fun  launchSpecificNetworkScope(coroutineScope: CoroutineScope, crossinline operation: CoroutineScope.() -> NetworkingEither<String?, *>):Flow<NetworkingEither<String?, *>> {

    return withContext(coroutineScope.coroutineContext){

        flow { emit(operation()) }
    }


}
